#!/usr/bin/python3
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import random
import scipy
from numpy.typing import NDArray
from python_tsp.heuristics import solve_tsp_local_search
from typing import Callable, Any

# pip3 install --upgrade python-tsp

# Can one generalize the TSP methods to solve 2d reductions too,
# much like an SOM does?


def num_pair_distance(x: int, y: int) -> float:
    if y <= x:
        return x - y
    else:
        return y - x


def euclidean_distance_color(
    item1: list[list[list[int]]], item2: list[list[list[int]]]
) -> float:
    return scipy.spatial.distance.euclidean(item1[0], item2[0])


def build_distance_matrix(
    collection: list[Any], distance_func: Callable[[Any, Any], Any]
) -> NDArray[NDArray[np.float64]]:
    return np.array(
        [[distance_func(item1, item2) for item2 in collection] for item1 in collection]
    )


if __name__ == "__main__":
    # Example with random numbers:
    data = np.array(range(1, 20))
    random.shuffle(data)
    print(f"Before: {data}")
    distance_matrix = build_distance_matrix(data, num_pair_distance)
    permutation, _distance = solve_tsp_local_search(distance_matrix)
    print(f"After: {data[permutation]}")

    # Example with random colors
    data = np.array(
        [
            [[random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]]
            for _ in range(50)
        ]
    )
    fig, ax = plt.subplots(ncols=2)
    ax[0].imshow(data)
    distance_matrix = build_distance_matrix(data, euclidean_distance_color)
    permutation, _distance = solve_tsp_local_search(distance_matrix)
    ax[1].imshow(data[permutation])
    plt.show()
