#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
SOM for Iris dataset
"""

import numpy as np
import matplotlib.pyplot as plt  # type: ignore


def closest_node(
    data: np.ndarray, t: int, lattice_map: np.ndarray, m_rows: int, m_cols: int
) -> tuple[int, int]:
    # (row,col) of map node closest to data[t]
    result = (0, 0)
    small_dist = np.float64(1.0e20)
    for i in range(m_rows):
        for j in range(m_cols):
            ed = euc_dist(v1=lattice_map[i][j], v2=data[t])
            if ed < small_dist:
                small_dist = ed
                result = (i, j)
    return result


def euc_dist(v1: np.ndarray, v2: np.ndarray) -> np.float64:
    return np.linalg.norm(v1 - v2)


def manhattan_dist(r1: int, c1: int, r2: int, c2: int) -> np.int64:
    return np.int64(np.abs(r1 - r2) + np.abs(c1 - c2))


def most_common(lst: list[np.int64], n: int) -> np.int64:
    # lst is a list of values 0 . . n
    if len(lst) == 0:
        return np.int64(-1)
    counts: np.ndarray = np.zeros(shape=n, dtype=int)
    for i in range(len(lst)):
        counts[lst[i]] += 1
    return np.argmax(counts)


def main() -> None:
    # 0. get started
    np.random.seed(1)
    Dim = 4
    Rows = 30
    Cols = 30
    RangeMax = Rows + Cols
    LearnMax = 0.5
    StepsMax = 5000

    # 1. load data
    print("\nLoading Iris data into memory \n")
    data_file = "iris_data_012.txt"
    data_x: np.ndarray = np.loadtxt(
        data_file, delimiter=",", usecols=range(0, 4), dtype=np.float64
    )
    data_y: np.ndarray = np.loadtxt(data_file, delimiter=",", usecols=[4], dtype=int)
    # option: normalize data

    # 2. construct the SOM
    print("Constructing a 30x30 SOM from the iris data")
    lattice_map = np.random.random_sample(size=(Rows, Cols, Dim))
    for s in range(StepsMax):
        if s % (StepsMax / 10) == 0:
            print("step = ", str(s))
        pct_left = 1.0 - ((s * 1.0) / StepsMax)
        curr_range = (int)(pct_left * RangeMax)
        curr_rate = pct_left * LearnMax

        t = np.random.randint(len(data_x))
        (bmu_row, bmu_col) = closest_node(
            data=data_x, t=t, lattice_map=lattice_map, m_rows=Rows, m_cols=Cols
        )
        for i in range(Rows):
            for j in range(Cols):
                if manhattan_dist(r1=bmu_row, c1=bmu_col, r2=i, c2=j) < curr_range:
                    # simplified learning rule
                    lattice_map[i][j] = lattice_map[i][j] + curr_rate * (
                        data_x[t] - lattice_map[i][j]
                    )
    print("SOM construction complete \n")

    # 3. construct U-Matrix (just plotting)
    print("Constructing U-Matrix from SOM")
    u_matrix: np.ndarray = np.zeros(shape=(Rows, Cols), dtype=np.float64)
    for i in range(Rows):
        for j in range(Cols):
            v = lattice_map[i][j]  # a vector
            sum_dists = np.float64(0.0)
            ct = 0

            if i - 1 >= 0:  # above
                sum_dists += euc_dist(v, lattice_map[i - 1][j])
                ct += 1
            if i + 1 <= Rows - 1:  # below
                sum_dists += euc_dist(v, lattice_map[i + 1][j])
                ct += 1
            if j - 1 >= 0:  # left
                sum_dists += euc_dist(v, lattice_map[i][j - 1])
                ct += 1
            if j + 1 <= Cols - 1:  # right
                sum_dists += euc_dist(v, lattice_map[i][j + 1])
                ct += 1

            u_matrix[i][j] = sum_dists / ct
    print("U-Matrix constructed \n")

    # display U-Matrix
    plt.imshow(u_matrix, cmap="gray")
    # black = close = clusters
    plt.show()

    # 4. because the data has labels, another possible visualization:
    # associate each data label with a map node
    print("Associating each data label with one map node ")
    mapping: np.ndarray = np.empty(shape=(Rows, Cols), dtype=object)
    for i in range(Rows):
        for j in range(Cols):
            mapping[i][j] = []

    for t in range(len(data_x)):
        (m_row, m_col) = closest_node(
            data=data_x, t=t, lattice_map=lattice_map, m_rows=Rows, m_cols=Cols
        )
        mapping[m_row][m_col].append(data_y[t])

    label_map: np.ndarray = np.zeros(shape=(Rows, Cols), dtype=int)
    for i in range(Rows):
        for j in range(Cols):
            label_map[i][j] = most_common(lst=mapping[i][j], n=3)

    plt.imshow(label_map, cmap=plt.cm.get_cmap("terrain_r", 4))
    plt.colorbar()
    plt.show()


if __name__ == "__main__":
    main()
