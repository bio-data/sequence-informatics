#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
SOM for random color data.
Note, this will take a while to run.
It will drop tons of images in whatever directory you run it in.
Scroll through those images to see it's performance.
"""
import numpy as np
import matplotlib.pyplot as plt  # type: ignore

map_size = 20
n_iterations = 99100


def main() -> None:
    # The lattice (plottable as an image).
    # Each node has 3-dim weight vector (RGB).
    nodes = np.random.rand(map_size, map_size, 3)
    plt.imshow(nodes, interpolation="none")
    plt.savefig("000.png")

    # The data samples (3 features).
    # Each data point is a random 1-pixel color.
    iterations = np.random.rand(n_iterations, 3)

    # The training
    for i in range(1, n_iterations):
        train_one(nodes, iterations, i)
        if i % 10 == 0:
            plt.imshow(nodes, interpolation="none")
            plt.savefig(f"{i:03d}" + ".png")

    # The mapping phase is implicit on this one.
    # Normally, data would be mapped back to lattice last.


def train_one(nodes: np.ndarray, iterations: np.ndarray, i: int) -> None:
    bmu = best_matching_unit(nodes, iterations[i])
    # Learning rate, reduces by time/iteration
    L = learning_ratio(i)
    for x in range(map_size):
        for y in range(map_size):
            # coordinate of cell
            c = np.array([x, y])
            # Distance between cell and bmu
            d = np.linalg.norm(c - bmu)
            # Scaling factor for distance from bmu, reduces by distance
            S = learning_radius(i, d)
            # TODO clean up using numpy function
            for z in range(3):
                nodes[x, y, z] += L * S * (iterations[i, z] - nodes[x, y, z])


def best_matching_unit(
    nodes: np.ndarray, iteration: np.ndarray
) -> tuple[np.int64, np.int64]:
    """Finds the index of the node that best matches the data point."""
    # compute all norms (square)
    # TODO simplify using pure numpy
    norms = np.zeros((map_size, map_size))
    for i in range(map_size):
        for j in range(map_size):
            for k in range(3):
                norms[i, j] += (nodes[i, j, k] - iteration[k]) ** 2
    # then, choose the minimum one
    # argment with minimum element
    bmu = np.argmin(norms)
    # argmin returns just flat, serial index,
    # so convert it back using unravel_index
    return np.unravel_index(bmu, (map_size, map_size))


def learning_ratio(t: int) -> np.float64:
    """
    The exponential function is e^x,
    where e is a mathematical constant called Euler's number,
    approximately 2.718281.
    np.exp() calculates e^x for each value of x in your input array.
    As the argument to exp goes more negative, values get closer to 0.
    As the argument to exp goes more positive, values get larger.
    This return value shrinks as t goes up.
    """
    halflife = float(n_iterations / 4)
    initial = 0.1
    return initial * np.exp(-t / halflife)


def learning_radius(t: int, d: np.float64) -> np.float64:
    """
    d is distance from BMU (nearer produces bigger values),
    by increasing the return value.
    s scales the neighborhood down over time,
    by shrinking the return value.
    They both have an impact.
    """
    s = shrinking_neighborhood(t)
    return np.exp(-(d**2) / (2 * s**2))


def shrinking_neighborhood(t: int) -> np.float64:
    """
    Shrinks the neighborhood radius over t,
    by produces smaller return values as t increases.
    """
    halflife = float(n_iterations / 4)
    initial = float(map_size / 2)
    return initial * np.exp(-t / halflife)


if __name__ == "__main__":
    main()
